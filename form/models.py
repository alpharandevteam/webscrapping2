from django import forms
from django.db import models
import datetime

class Agenda_data(models.Model):
    nom_manifestation = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    date = models.CharField(max_length=100)
    image = models.CharField(max_length=100)
    description_url = models.CharField(max_length=100)
    def __str__(self):
        return self.url

class Agenda(models.Model):
    url = models.CharField(max_length=100)
    balise_du_boucle = models.CharField(max_length=100)
    attribut_du_boucle = models.CharField(max_length=100)
    valeur_attribut_du_boucle = models.CharField(max_length=100)
    balise_titre = models.CharField(max_length=100)
    balise_fille_du_titre = models.CharField(max_length=100)
    balise_description = models.CharField(max_length=100)
    attr_desc = models.CharField(max_length=100)
    value_attr_desc = models.CharField(max_length=100)
    balise_date = models.CharField(max_length=100)
    attr_date = models.CharField(max_length=100)
    value_attr_date = models.CharField(max_length=100)
    balise_de_la_categorie = models.CharField(max_length=100)
    attribut_de_la_categorie = models.CharField(max_length=100)
    valeur_attribut_categorie = models.CharField(max_length=100)
    def __str__(self):
        return self.url