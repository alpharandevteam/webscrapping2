from django import forms
import datetime
class ContactForm(forms.Form):
    url = forms.CharField(max_length=100)
    balise_du_boucle = forms.CharField(max_length=100)
    attribut_du_boucle = forms.CharField(max_length=100)
    valeur_attribut_du_boucle = forms.CharField(max_length=100)
    balise_titre = forms.CharField(max_length=100)
    balise_fille_du_titre = forms.CharField(max_length=100)

    balise_description = forms.CharField(max_length=100)
    attr_desc = forms.CharField(max_length=100)
    value_attr_desc = forms.CharField(max_length=100)

    balise_date = forms.CharField(max_length=100)
    attr_date = forms.CharField(max_length=100)
    value_attr_date = forms.CharField(max_length=100)

    balise_de_la_categorie = forms.CharField(max_length=100)
    attribut_de_la_categorie = forms.CharField(max_length=100)
    valeur_attribut_categorie = forms.CharField(max_length=100)