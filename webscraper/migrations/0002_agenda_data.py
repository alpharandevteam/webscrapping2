# Generated by Django 2.2.1 on 2019-06-03 10:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webscraper', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Agenda_data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom_manifestation', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=100)),
                ('category', models.CharField(max_length=100)),
                ('date', models.CharField(max_length=100)),
                ('image', models.CharField(max_length=100)),
                ('description_url', models.CharField(max_length=100)),
            ],
        ),
    ]
