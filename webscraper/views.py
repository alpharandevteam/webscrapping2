from .form import ContactForm
from .models import Agenda_data
from django.shortcuts import render
from django.http import HttpResponse
import AdvancedHTMLParser
from urllib.request import Request, urlopen, urlparse
from django.shortcuts import render
from django.http import HttpResponse
import re
from bs4 import BeautifulSoup
import collections
import re, datetime, dateparser
from dateparser.search import search_dates

def submit(request):
    mois_bList = [
        'jan', 'janvier', 'Jan', 'Janvier', 'jan.', 'Jan.',
        'fév', 'fevrier', 'février', 'Fév', 'Février', 'Fev', 'fev', 'fév.', 'Fév.', 'Fev.', 'fev.',
        'mars', 'Mars',
        'avr', 'avril', 'Avr', 'Avril', 'avr.', 'Avr.',
        'mai', 'Mai',
        'juin', 'Juin', 'JUIN',
        'juil', 'juillet', 'Juil', 'Juillet', 'juil.', 'Juil.','jul'
        'août', 'Août',
        'sept', 'septembre', 'Sept', 'Septembre', 'sept.', 'Sept.',
        'oct', 'octobre', 'Oct', 'Octobre', 'oct.', 'Oct.',
        'nov', 'novembre', 'Nov', 'Novrembre', 'nov.', 'Nov.',
        'déc', 'décembre', 'Déc', 'Décembre', 'Dec', 'dec', 'déc.', 'Déc.', 'dec.', 'Dec.'
    ]

    # liste des mois_b en anglais
    month = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'Jun',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]
    jan = ['jan', 'janvier', 'Jan', 'Janvier', 'jan.', 'Jan.', ]
    fev = ['fév', 'fevrier', 'février', 'Fév', 'Février',
           'Fev', 'fev', 'fév.', 'Fév.', 'Fev.', 'fev.', ]
    mar = ['mars', 'Mars', ]
    avr = ['avr', 'avril', 'Avr', 'Avril', 'avr.', 'Avr.', ]
    mai = ['mai', 'Mai', ]
    juin = ['juin', 'Juin', 'JUIN']
    juil = ['juil', 'juillet', 'Juil', 'Juillet', 'juil.', 'Juil.','jul' ]
    aout = ['août', 'Août', ]
    sept = ['sept', 'septembre', 'Sept', 'Septembre', 'sept.', 'Sept.', ]
    octo = ['oct', 'octobre', 'Oct', 'Octobre', 'oct.', 'Oct.', ]
    nov = ['nov', 'novembre', 'Nov', 'Novrembre', 'nov.', 'Nov.', ]
    dec = ['déc', 'décembre', 'Déc', 'Décembre',
           'Dec', 'dec', 'déc.', 'Déc.', 'dec.', 'Dec.']
    form = request.POST
    url = form.get('url')
    attribut_du_boucle = form.get('attribut_du_boucle')
    balise_du_boucle = form.get('balise_du_boucle')
    valeur_attribut_du_boucle = form.get('valeur_attribut_du_boucle')

    balise_titre = form.get('balise_titre')
    attr_tittle = form.get('attr_tittle')
    value_attr_titre = form.get('value_attr_titre')
    balise_fille_du_titre = form.get('balise_fille_du_titre')

    balise_description = form.get('balise_description')
    attr_desc = form.get('attr_desc')
    value_attr_desc = form.get('value_attr_desc')
    balise_date = form.get('balise_date')
    attr_date = form.get('attr_date')
    value_attr_date = form.get('value_attr_date')

    categorie_attr = form.get('attribut_de_la_categorie')
    value_attr_categ = form.get('valeur_attribut_categorie')
    categ_balise = form.get('balise_de_la_categorie')

    date = form.get('date')
    option = form.get('option_date')
    format_date = form.get('format_date')
    ordre_date = form.get('ordre_date')
    spiltby = form.get('spiltby')

    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    soup = BeautifulSoup(webpage, "html.parser")
    div = soup.find_all(balise_du_boucle, {
                        attribut_du_boucle: valeur_attribut_du_boucle})
    starttime = ''
    for i in div:
        if not i.find(balise_titre,{attr_tittle:value_attr_titre}) is None and balise_titre != "":
            tittle = i.find(balise_titre,{attr_tittle:value_attr_titre})
            if tittle.findChildren(balise_fille_du_titre):
                tittle = tittle.get_text().strip()
        elif not i.find(balise_titre) is None:
            tittle = i.find(balise_titre)
            if tittle.findChildren(balise_fille_du_titre):
                tittle = tittle.get_text().strip()
        if not i.find(balise_description, {attr_desc: value_attr_desc}) is None and attr_desc != '':
            description = i.find(balise_description, {attr_desc: value_attr_desc}).get_text().strip()
        elif not i.find(balise_description) is None:
            description = i.find(balise_description).get_text().strip()

        if not i.find(categ_balise, {categorie_attr: value_attr_categ}) is None and categ_balise != '':
            categorie = i.find(
                categ_balise, {categorie_attr: value_attr_categ})
            categorie = categorie.get_text().strip()

        elif not i.find(categ_balise) is None:
            categorie = i.find(categ_balise).get_text().strip()

        else:
            categorie = "autre"

        if not i.find('img')['src'] is None:
            img = i.find('img')['src']
        date_fin ='000-00-00'
        if not i.find(balise_date, {attr_date: value_attr_date}) is None:
            temps = i.find(balise_date, {attr_date: value_attr_date})
            if option != '':
                if option == '1':
                    date_s = ''
                    if not temps.find('span') is None:
                        temps.find('span').decompose()
                    date_s = temps.get_text()
                    starttime = date_s

                elif option == '2':
                    date_d = []
                    date_s = ''
                    for x in temps:
                        date_d.append(x.get_text())
                    for x in date_d:
                        date_s = date_s+" "+x
                    starttime = date_s

            if format_date != '' and temps != '' and ordre_date != '':
                if format_date == 'ymd_full':

                    temps = str(temps.get_text())
                    date_brute = temps.lower()
                    splited = date_brute.split(spiltby)
                    jour_a =''
                    mois_a = ''
                    annee_a = ''
                    date_fin = "000-00-00"
                    for i in splited:
                        mois_b = i
                        if mois_b in jan:
                            mois_a = '01'
                        elif mois_b in fev:
                            mois_a = '02'
                        elif mois_b in mar:
                            mois_a = '03'
                        elif mois_b in avr:
                            mois_a = '04'
                        elif mois_b in mai:
                            mois_a = '05'
                        elif mois_b in juin:
                            mois_a = '06'
                        elif mois_b in juil:
                            mois_a = '07'
                        elif mois_b in aout:
                            mois_a = '08'
                        elif mois_b in sept:
                            mois_a = '09'
                        elif mois_b in octo:
                            mois_a = '10'
                        elif mois_b in nov:
                            mois_a = '11'
                        elif mois_b in dec:
                            mois_a = '12'

                        if len(i) <= 2:
                            try:
                                a = int(i)
                                if jour_a == '':
                                    if len(i) == 1:
                                        j = '0'+str(i)
                                        jour_a = j
                                    else:
                                        jour_a = str(i)
                            except:
                                pass
                        if len(i) == 4:
                            try:
                                b = int(i)
                                if annee_a =='':
                                    annee_a = str(i)
                            except:
                                pass
                    if annee_a == '':

                        date_all = datetime.datetime.now()
                        annee_null = date_all.year
                        annee_a = str(annee_null)
                    

                    if jour_a !='' or mois_a !='':     
                        date_debut = annee_a + '-'+mois_a+'-'+jour_a
                    else:
                        try:
                            a = search_dates(temps)
                            test1 = a[0]
                            date_debut = str(test1[1])
                            date_debut_brut = date_debut.split(' ')
                            date_debut = date_debut_brut[0]
                        except:
                            date_debut = "0000-00-00"
                            pass
                if format_date == 'ymd_short':
                    date_debut = '000-00-00'
                if format_date == 'texte':
                    date_debut = '000-00-00'
        else:
            date_debut = '000-000-000'
        description_url = ""
        saved = Agenda_data(nom_manifestation=tittle, description=description, category=categorie,date_debut=date_debut, date_fin=date_fin, image=img, description_url=description_url)
        if saved:
            saved.save()
    return HttpResponse('ok')


def contact(request):
    form = ContactForm(request.POST or None)
    return render(request, 'contact.html', locals())